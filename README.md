# Timetrack

Simple timetrack app for GNOME

<a href='https://flathub.org/apps/details/net.danigm.timetrack'>
    <img width='240' alt='Download on Flathub' src='https://flathub.org/assets/badges/flathub-badge-en.png'/>
</a>

![Screenshot1](https://gitlab.gnome.org/danigm/timetrack/raw/master/screenshots/timetrack1.png)
![Screenshot2](https://gitlab.gnome.org/danigm/timetrack/raw/master/screenshots/timetrack2.png)
![Screenshot3](https://gitlab.gnome.org/danigm/timetrack/raw/master/screenshots/timetrack3.png)

## Build instructions

This project uses the meson build system. To build you should only follow
these steps:

 1. meson build
 1. ninja -C build
 1. ninja -C build install

This is a gtk python projects and this is the list of deps:

 1. python 3
 1. Gtk and pyGobject
 1. libhandy 1

## How to contribute

This project uses flatpak for development build. The recommended way to build
for development is to use gnome-builder. Builder will find the flatpak json and
you will be able to run the master version pressing the play button. Builder
will install all development dependencies in the flatpak.

from datetime import datetime

from gettext import gettext as _
from gi.repository import Adw, Gtk, GObject, GLib

from timetrack.activity_headerbar import ActivityHeaderBar
from timetrack.report_headerbar import ReportHeaderBar


@Gtk.Template(resource_path="/net/danigm/timetrack/headerbar.ui")
class TimetrackHeaderBar(Adw.Bin):
    __gtype_name__ = "TimetrackHeaderBar"

    header_stack = Gtk.Template.Child()
    report_headerbar = Gtk.Template.Child()
    activity_headerbar = Gtk.Template.Child()

    @GObject.Signal()
    def current_changed(self):
        return

    @GObject.Signal()
    def startstop(self):
        return

    @GObject.Signal()
    def inserted_text(self):
        return

    @GObject.Signal()
    def deleted_text(self):
        return

    @GObject.Property(type=GObject.TYPE_PYOBJECT)
    def window(self):
        return self._window

    @window.setter
    def window(self, value):
        self._window = value
        self.report_headerbar.window = self._window

    @GObject.Property(type=str)
    def current_timer_text(self):
        return self.activity_headerbar.current_timer.get_text()

    @current_timer_text.setter
    def current_timer_text(self, value):
        self.activity_headerbar.current_timer.set_text(value)

    @GObject.Property(type=str)
    def activity(self):
        return self.activity_headerbar.activity_entry.get_text()

    @activity.setter
    def activity_text(self, value):
        self.activity_headerbar.activity_entry.set_text(value)

    @GObject.Property()
    def current_day(self):
        return self.activity_headerbar.current_day.get_date()

    @current_day.setter
    def current_day(self, value):
        self.activity_headerbar.current_day.select_day(value)

    @GObject.Property()
    def current_name(self):
        return self.activity_headerbar.current_name.get_text()

    @current_name.setter
    def current_name(self, value):
        self.activity_headerbar.current_name.set_text(value)

    @GObject.Property()
    def current_comments(self):
        comments_buf = self.activity_headerbar.current_comments.get_buffer()
        comments_start = comments_buf.get_start_iter()
        comments_end = comments_buf.get_end_iter()
        comments = comments_buf.get_text(comments_start, comments_end, False)
        return comments

    @current_comments.setter
    def current_comments(self, value):
        self.activity_headerbar.current_comments.get_buffer().set_text(value)

    @GObject.Property()
    def current_hour(self):
        return int(self.activity_headerbar.current_hour.get_value())

    @current_hour.setter
    def current_hour(self, value):
        self.activity_headerbar.current_hour.set_value(value)

    @GObject.Property()
    def current_minute(self):
        return int(self.activity_headerbar.current_minute.get_value())

    @current_minute.setter
    def current_minute(self, value):
        self.activity_headerbar.current_minute.set_value(value)

    @GObject.Property()
    def current_date(self):
        date = self.current_day
        return datetime(date.get_year(),
                        date.get_month(),
                        date.get_day_of_month(),
                        self.current_hour,
                        self.current_minute)

    def update_current_popup(self, activity):
        self.current_name = activity.name
        self.current_hour = activity.start.hour
        self.current_minute = activity.start.minute

        timetuple = activity.start.timetuple()[0:6]
        dt = GLib.DateTime.new_local(*timetuple)
        self.current_day = dt

        self.current_comments = activity.comments

    @Gtk.Template.Callback()
    def change_current(self, *args, **kwargs):
        self.emit("current_changed")

    @Gtk.Template.Callback()
    def startstop_activity(self, *args, **kwargs):
        self.emit("startstop")

    @Gtk.Template.Callback()
    def inserted_text_cb(self, *args, **kwargs):
        self.emit("inserted_text")

    @Gtk.Template.Callback()
    def deleted_text_cb(self, *args, **kwargs):
        self.emit("deleted_text")

    def show_report(self):
        self.header_stack.set_visible_child_name("report-header")

    def show_list(self):
        self.header_stack.set_visible_child_name("activity-header")
        if not self.activity_headerbar.is_working():
            self.activity_headerbar.activity_entry.grab_focus()

    def set_working(self):
        self.activity_headerbar.set_working()

    def set_waiting(self):
        self.activity_headerbar.set_waiting()
        self.activity_headerbar.activity_entry.grab_focus()

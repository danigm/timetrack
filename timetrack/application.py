import os
import logging
import gi
import sys
gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')
from gi.repository import Adw, GLib, Gio, Gtk
from timetrack.main_window import MainWindow
from timetrack.db import DB


class Application(Gtk.Application):
    window = NotImplemented
    file_list = []
    development_mode = False
    application_id = "net.danigm.timetrack"
    db = NotImplemented
    localedir = None

    def __init__(self, *args, **kwargs):
        self.localedir = kwargs.pop('localedir')
        Adw.init()
        super().__init__(
            *args, application_id=self.application_id, flags=Gio.ApplicationFlags.HANDLES_OPEN)
        self.window = None
        dbpath = os.path.join(GLib.get_user_data_dir(), "timetrack.sqlite3")
        self.db = DB(dbpath)

    def do_startup(self):
        Gtk.Application.do_startup(self)
        GLib.set_application_name("Timetrack")
        GLib.set_prgname("Timetrack")

        self.assemble_application_menu()

    def do_activate(self):
        if not self.window:
            self.window = MainWindow(
                localedir=self.localedir,
                application=self, title="Timetrack",
                icon_name=self.application_id)

            self.window.application = self
            self.window.add_actions()
            self.window.load_last()
            self.add_global_accelerators()

        self.window.present()

    def do_shutdown(self):
        self.db.remove_deleted()
        self.db.close()
        Gtk.Application.do_shutdown(self)

    def get_logger(self):
        logger = logging.getLogger()
        if self.development_mode is True:
            logging.basicConfig(format="%(asctime)s | %(levelname)s | %(message)s", datefmt='%d-%m-%y %H:%M:%S', level=logging.DEBUG)
        else:
            logging.basicConfig(format="%(asctime)s | %(levelname)s | %(message)s", datefmt='%d-%m-%y %H:%M:%S', level=logging.INFO)
        return logger

    def assemble_application_menu(self):
        about_action = Gio.SimpleAction.new("about", None)
        about_action.connect("activate", self.on_about_menu_clicked)

        quit_action = Gio.SimpleAction.new("quit", None)
        quit_action.connect("activate", self.on_quit_menu_clicked)

        shortcuts_action = Gio.SimpleAction.new("shortcuts", None)
        shortcuts_action.connect("activate", self.on_shortcuts_menu_clicked)

        self.add_action(about_action)
        self.add_action(quit_action)
        self.add_action(shortcuts_action)

    def on_about_menu_clicked(self, action, param):
        builder = Gtk.Builder()
        builder.add_from_resource("/net/danigm/timetrack/about_dialog.ui")
        about_dialog = builder.get_object("about_dialog")
        about_dialog.set_modal(True)
        if self.window is not NotImplemented:
            about_dialog.set_transient_for(self.window)
        about_dialog.present()

    def on_quit_menu_clicked(self, action, param):
        self.quit()

    def on_shortcuts_menu_clicked(self, action, param):
        builder = Gtk.Builder()
        builder.add_from_resource("/net/danigm/timetrack/shortcuts_overview.ui")
        shortcuts_overview = builder.get_object("shortcuts_overview")
        if self.window is not NotImplemented:
            shortcuts_overview.set_transient_for(self.window)
        shortcuts_overview.show()

    def add_global_accelerators(self):
        self.set_accels_for_action("app.shortcuts", ["<Control>question"])

        self.set_accels_for_action("app.report", ["<Control>r"])
        self.set_accels_for_action("app.list", ["<Control>l"])

        self.set_accels_for_action("app.activity.start", ["<Control>s"])

        # edit
        self.set_accels_for_action("app.new", ["<Control>n"])
        self.set_accels_for_action("app.edit", ["<Control>e"])
        self.set_accels_for_action("app.remove", ["Delete"])
        self.set_accels_for_action("app.restore", ["<Control>z"])

        # report
        self.set_accels_for_action("app.report-left", ["<Control>Left"])
        self.set_accels_for_action("app.report-right", ["<Control>Right"])
        self.set_accels_for_action("app.report-today", ["<Control>Down"])

        self.set_accels_for_action("app.report-day", ["<Alt>1"])
        self.set_accels_for_action("app.report-week", ["<Alt>2"])
        self.set_accels_for_action("app.report-month", ["<Alt>3"])

        self.set_accels_for_action("app.report-filter", ["<Control>f"])

        self.set_accels_for_action("app.quit", ["<Control>q"])


if __name__ == "__main__":
    app = Application()
    app.run(sys.argv)


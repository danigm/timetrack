from gettext import gettext as _
from gi.repository import Adw, Gtk, GObject, GLib


@Gtk.Template(resource_path="/net/danigm/timetrack/report_headerbar.ui")
class ReportHeaderBar(Adw.Bin):
    __gtype_name__ = "ReportHeaderBar"

    report_stack_switcher = Gtk.Template.Child()
    report_export = Gtk.Template.Child()

    @property
    def application():
        return Gio.Application.get_default()

    @GObject.Property(type=GObject.TYPE_PYOBJECT)
    def window(self):
        return self._window

    @window.setter
    def window(self, value):
        self._window = value
        self.report_stack_switcher.set_stack(self._window.report_stack)

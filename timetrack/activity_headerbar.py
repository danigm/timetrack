from gettext import gettext as _
from gi.repository import Adw, Gtk, GObject, GLib


@Gtk.Template(resource_path="/net/danigm/timetrack/activity_headerbar.ui")
class ActivityHeaderBar(Adw.Bin):
    __gtype_name__ = "ActivityHeaderBar"

    activity_toggle = Gtk.Template.Child()
    activity_entry = Gtk.Template.Child()
    activity_stack = Gtk.Template.Child()
    button_stack = Gtk.Template.Child()
    start_button = Gtk.Template.Child()
    stop_button = Gtk.Template.Child()

    current_name = Gtk.Template.Child()
    current_hour = Gtk.Template.Child()
    current_minute = Gtk.Template.Child()
    current_day = Gtk.Template.Child()
    current_timer = Gtk.Template.Child()
    current_comments = Gtk.Template.Child()

    @GObject.Signal()
    def current_changed(self):
        return

    @GObject.Signal()
    def startstop(self):
        return

    @GObject.Signal()
    def inserted_text(self):
        return

    @GObject.Signal()
    def deleted_text(self):
        return

    @property
    def application():
        return Gio.Application.get_default()

    @GObject.Property(type=str)
    def current_timer_text(self):
        return self.current_timer.get_text()

    @current_timer_text.setter
    def current_timer_text(self, value):
        self.current_timer.set_text(value)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.current_comments.get_buffer().connect('changed', self.change_current)
        self.activity_entry_buffer = self.activity_entry.get_buffer()
        self.activity_entry_buffer.connect_after("inserted-text", self.inserted_text_signal)
        self.activity_entry_buffer.connect_after("deleted-text", self.deleted_text_signal)

    def update_current_popup(self, activity):
        self.current_name.set_text(activity.name)
        self.current_hour.set_value(activity.start.hour)
        self.current_minute.set_value(activity.start.minute)

        timetuple = activity.start.timetuple()[0:6]
        dt = GLib.DateTime.new_local(*timetuple)
        self.current_day.select_day(dt)

        self.current_comments.get_buffer().set_text(activity.comments)

    @Gtk.Template.Callback()
    def change_current(self, *args, **kwargs):
        self.emit("current_changed")

    @Gtk.Template.Callback()
    def startstop_activity(self, *args, **kwargs):
        self.emit("startstop")

    def set_working(self):
        self.activity_stack.set_visible_child_name("working")
        self.button_stack.set_visible_child_name("working")
        self.activity_toggle.set_label(self.activity_entry.get_text())

    def set_waiting(self):
        self.activity_stack.set_visible_child_name("waiting")
        self.button_stack.set_visible_child_name("waiting")

    def is_working(self):
        return self.activity_stack.props.visible_child_name == "working"

    def inserted_text_signal(self, *args, **kwargs):
        self.emit("inserted_text")

    def deleted_text_signal(self, *args, **kwargs):
        self.emit("deleted_text")

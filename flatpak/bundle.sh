#!/bin/bash

flatpak-builder --repo=repo --force-clean timetrack net.danigm.timetrack-release.json
flatpak build-bundle repo net.danigm.timetrack.flatpak net.danigm.timetrack
flatpak uninstall -y --user net.danigm.timetrack
flatpak install -y --user net.danigm.timetrack.flatpak

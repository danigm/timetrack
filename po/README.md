# Translations

Timetrack uses meson to build and the i18n module:
https://mesonbuild.com/Localisation.html

## Update pot file:

    meson compile -C build timetrack-pot

## Update all translations:

    meson compile -C build timetrack-update-po
